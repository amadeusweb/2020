<?php
include_once '../../amadeus/framework/core.php';
include_once '_functions.php';
cs_var('local', $local = $_SERVER['HTTP_HOST'] ==='localhost');

load_account();

$auth = cs_var('auth');
$course = explode('/', $_GET['file'])[0];
$courses = auth_courses();

if (!$courses[$course])
	print_auth_error('You do not have access to this course');
else
	echo file_get_contents(__DIR__ . '/_' . $_GET['file'] . '-protected.txt');
?>
