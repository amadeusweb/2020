<?php
$local = $_SERVER['HTTP_HOST'] ==='localhost';
return [
	'theme' => 'flexor',
	'styles' => ['flexor-modifs', 'styles'],
	'scripts' => ['lyrics'],
	//NB: NOT YET INCLUDED IN FOOTER 'scripts' => ['scripts'],
	'head_hooks' => $local ? [] : [__DIR__ . '/_ga.php', __DIR__ . '/_yeps.php'],
	'support_page_parameters' => true,
	'hide_topbar' => true,
		'phone' => '+919841223313', 'whatsapp' => '+919841223313',
		'email' => 'imran@yieldmore.org',
		'timings' => '24 / 7',
		'cta' => '<a href="register" class="scrollto">Register Now</a>',
	'rich_footer' => [
		'title' => 'YieldMore.org',
		'description' => '<b>All LOVE to the World Family</b> - Jagat Kula Ki Prem',
		'links' => [
			'YieldMore.org' => 'https://yieldmore.org/',
			'YM Media' => 'https://yieldmore.org/media/" target="_blank',
			'YM Legacy' => 'https://legacy.yieldmore.org/" target="_blank',
		],
		'links2' => [
			//'Information Bureau' => 'https://yieldmore.org/information-bureau',
			'Creative Expression' => 'https://yieldmore.org/creative-expression/',
			'Healing' => 'https://yieldmore.org/healing/',
			'WoW Initiative' => 'https://yieldmore.org/wow/',
		],
		'phone' => '+919841223313', 'whatsapp' => '+919841223313',
		'email' => base64_encode('imran@yieldmore.org'),
		'address' => '', //Flat No 6, Rams Apartments,<br /> 5/2A Sir Madhavan Nair Road,<br /> Mahalingapuram,<br /> Chennai 600034',
		'email_subject' => '?subject=YM+Enquiry',
		'facebook_widget1' => 'yieldmore.org',
		'start_year' => 2013,
		'social' => [
			['name' => 'twitter', 'link' => 'https://twitter.com/YieldMoreOrg'],
			['name' => 'facebook', 'link' => 'https://www.facebook.com/YieldMoreOrg/'],
			//['name' => 'facebook', 'link' => 'https://www.facebook.com/groups/YieldMore/about/'],
			['name' => 'instagram', 'link' => 'https://instagram.com/imran_ym'],
			['name' => 'instagram', 'link' => 'https://instagram.com/yieldmore_love'],
			['name' => 'youtube', 'link' => 'https://www.youtube.com/channel/UCESPy4vMsnv3htBqvHJh51Q/'],
			['name' => 'youtube', 'link' => 'https://www.youtube.com/channel/UC_iHhVADe1oSjP3oAi5bnnw/'],
		]
	],
];
?>