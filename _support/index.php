<section id="<?php echo cs_var('node'); ?>">
	<?php page_banner(); ?>
	<div class="container">
		<?php page_about() ?>
		<div class="row">
			<div class="<?php echo cs_var('sub-site-width'); ?>">
			<?php if (isset($_GET['signup'])) include_once 'requests/form.php';
			else echo wpautop(file_get_contents(__DIR__ . '/' . cs_var('node') . '.txt'));?>
			</div>
		
			<div class="<?php echo cs_var('sub-site-right-col-width')?> sub-site">
				<div class="related-links"><?php sub_site_heading(); ?></div>
				<p>We propose a system of #DirectDonations, mentioning supporters, organizations and needy people all in this one section.</p><p><a class="btn btn-primary" href="<?php echo cs_var('url'); ?>support/?signup=1">Signup Here</a></p>
				<?php menu(['folder' => true]) ?>
			</div>
		</div>
	</div>
</section>
