<!--Submission To YieldMore.org dated 02 Dec 2020-->

<h2>Basic</h2>

<u>Name</u>
Radhakrishnan Subbuduraisamy

<u>Type</u>
open forum

<u>Byline</u>
A forum for sustainable development

<u>City, Country</u>
Chennai

<h2>Details</h2>

<u>Website</u>
<a href="https://planetblue.org.in" target=" blank">planetblue.org.in</a>

<u>Year Founded</u>
2020

<h3>Short Introduction</h3>
This Forum aims to bring in more people to the sustainable development arena to respond to the mounting problems ahead. And also tries for the inclusion of sustainability in existing organisations' process.

<h3>Causes</h3>
Sustainable development, Water conservation, Food, Energy, Environment, Health and Education.

<h2>More Details</h2>

<u>Regions Active</u>
Tamil Nadu

<u>Years Active</u>
1

<u>Team Size</u>
20

<h2>Contact</h2>

<u>Contact Person</u>
Radhakrishnan

<u>Email</u>
<a href="planetblueforum@gmail.com">planetblueforum@gmail.com</a>

<u>Mobile</u>
<a href="tel:+919789840630">+919789840630</a>

<h2>Financials</h2>

<h3>Next Years Expense Projection</h3>
5 lakhs for the financial year 2021 April - 2022 March. Covers Co-ordinator Salary and Admin expenses.

<h2>Call To Action</h2>

<h3>Candid Message</h3>
If our children and the future generation have to live happily in this Earth, let's start working on Sustainable development right now!

<h3>Non Financial Help Sought</h3>
Volunteers, Experts, Networking

<h2>Internal</h2>

<u>Date Connected To YM</u>
02/12/2020

