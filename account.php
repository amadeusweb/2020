<div class="container">
<a class="btn btn-primary" href="<?php echo cs_var('url');?>account/">Account Page</a>
<?php

if (!cs_var('simulate_auth')) {
$auth = cs_var('auth');

if ($auth->isLoggedIn()) {
	print_auth_message('<b>' . $auth->getEmail() . '</b> <a style="text-decoration: underline; display: inline-block; margin-bottom: 0;" href="'. cs_var('url') . 'account/?logout=1">Logout</a>');
}
else {
	//echo 'User is not signed in yet';
	if (isset($_GET['signin'])) {
		if (!cs_var('signin_result')) build_signin();
	} else if (isset($_GET['verify_email'])) {
		if (auth_verify()) build_signin();
	} else if (!(isset($_GET['signup']) && auth_signup())) {
		build_signup();
		build_signin();
	} else {
		build_signin();
	}
}

}

print_course_info();
?>
</div>
