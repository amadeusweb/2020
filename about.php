<section id="<?php echo cs_var('node'); ?>">
	<?php page_banner(); ?>
<div class="container">
	<?php page_about() ?>
Each of our ideas and initiatives are carefully crafted and baked to perfection! Now, they just need your involvement to help them take the world by storm.<br><br>

Softlaunched on 12th May 2020, YieldMore.org is a link between people, ideas and movements. We are an information portal and a platform for ideas.<br><br>

<a name="mission">
<h3>Mission</h3>
We want to help people heal, grow and thrive, learning to be excellent to each other and to nature.<br><br>

<h3>Plan</h3>
We love problem solving and finding the best people to help you achieve your life goals.<br><br>

<h3>Vision</h3>
We believe there's enough goodness around but it needs to spread like wildfire. We are here to catalyse that.<br><br>

<h3>Care</h3>
Ours is a Conscious Business, thats why we take time to understand everyone in order to serve to the best.<br><br>

<hr />
<h3>Join a wave of the future! (2018 December)</h3>

<p>Volunteer for a cause towards sharing knowledge, teaching children or skilling the workforce. This vertical simply called <strong>Learn</strong> has the journal <strong>Sunlight</strong> and includes a focus on Special Needs children and adults as well as life coaching.</p>

<p>Learn of the subtler laws that manifest in better health of body, mind and soul as you discover programs for yourself, train in alternative healing practices and programmes. Our <strong>school for positive thinking</strong> has many adherents and Medico features the work or all Healers and sundry. The journal is <strong>Serenity</strong>.</p>

<p>Heal the world as the <strong><a href="https://legacy.yieldmore.org/brother">thousands of loving movements</a></strong> do. These are not-for-profits and forward thinking businesses changing slowly and patiently the very fabric of society. Enliven and enjoin a social cause as do these many <strong><a href="https://legacy.yieldmore.org/miu">#MovementsInUnison</a></strong>. The journal <strong>Symphony</strong> tells their story.</p>

<p>That sums up our 3 channels Learn Heal and Share. The full progression includes Express Love Build and finally YieldMore.</p>

<p>Begun in 2013 as a hobbyist blog by Imran Ali Namazi and a wordpress customization called biblios to publish books and other literary works, today YieldMore.org is on the threshold of the world, just where it was meant to be.</p>
</div>
</section>
