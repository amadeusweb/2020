<?php
cs_var('drive-path', cs_var('path') . '/downloads/');
cs_var('drive-url', cs_var('url') . '/downloads/');

function drive_list_folders() {
	$path = cs_var('drive-path');
	$name = cs_var('page_parameter1');
	foreach (scandir($path) as $fol) {
		if ($fol == '.' || $fol == '..' || $fol == 'readme.txt') continue;
		$class = $name == $fol ? ' class="selected"' : '';
		echo sprintf('<a href="%s/"%s>%s</a><br />', cs_var('url') . 'drive/' . $fol, $class, str_replace('-', ' ', $fol));
	}
}

function drive_folder_toc() {
	$name = cs_var('page_parameter1');
	if (!$name) return;
	$base = cs_var('url') . 'drive/' . $name . '/';

	echo sprintf('<a href="%s" class="heading">TOC of %s</a>' . cs_var('brnl'), $base, $name);
	$fol = cs_var('drive-path') . $name;

	$name2 = cs_var('page_parameter2');
	$last = '';
	foreach (scandir($fol) as $fil) {
		if ($fil == '.' || $fil == '..' || $fil == 'index.txt' || $fil == 'index.html') continue;
		$ext = pathinfo($fil, PATHINFO_EXTENSION);
		$slug = basename($fil, '.' . $ext);
		if ($last == $slug || endsWith($slug, '-english')) continue;
		$last = $slug;
		$class = $name2 == $slug ? ' class="selected"' : '';
		echo sprintf('<a href="%s/"%s>%s</a><br />', $base . $slug, $class, str_replace('-', ' ', $slug));
	}
}

function drive_render() {
	if (!cs_var('page_parameter1')) { echo wpautop(file_get_contents(cs_var('drive-path') . 'readme.txt')); return; }
	if (!cs_var('page_parameter2')) {
		$base = cs_var('drive-path') . cs_var('page_parameter1') . '/index';
		if (file_exists($base . '.html'))
			echo file_get_contents($base . '.html');
		else
			echo wpautop(file_get_contents($base . '.txt'));
		return;
	}

	//actual fun begins
	$base = cs_var('drive-path') . cs_var('page_parameter1') . '/' . cs_var('page_parameter2');
	$url = cs_var('drive-url') . cs_var('page_parameter1') . '/' . cs_var('page_parameter2');
	if (file_exists($base . '.mp3'))
		echo sprintf('<audio class="drive-mp3 player-default" height="27" preload="none" controls><source src="%s" type="audio/mp3"></audio>', $url . '.mp3');
	if (file_exists($base . '-english.mp3'))
		echo sprintf('<audio class="drive-mp3 player-english" height="27" preload="none" controls><source src="%s" type="audio/mp3"></audio>', $url . '-english.mp3');
	if (file_exists($base . '.html'))
		echo file_get_contents($base . '.html');
	else if (file_exists($base . '.txt'))
		echo wpautop(file_get_contents($base . '.txt'));
}

function drive_link() {
	echo sprintf('<a href="%s" class="heading">%s</a>' . cs_var('brnl'), cs_var('url') . 'drive/', 'YM Drive');
}
?>

<section id="<?php echo cs_var('node'); ?>">
	<?php page_banner(); ?>
	<div class="container">
		<?php page_about() ?>
		<div class="row">
			<div class="<?php echo cs_var('sub-site-width'); ?>">
			<?php drive_render();?>
			</div>
		
			<div class="<?php echo cs_var('sub-site-right-col-width')?>">
				<div class="related-links">
					<?php drive_link(); ?>
					<?php drive_list_folders(); ?>
				</div>
				<hr />
				<div class="related-links"><?php drive_folder_toc(); ?></div>
			</div>
		</div>
	</div>
</section>
