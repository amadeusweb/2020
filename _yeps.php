<?php if (cs_var('embed') || array_search(cs_var('node'), cs_var('pre-banner-excludes')) !== false) return; ?>
<!-- Yeps {literal} -->
<script>
    (function (y, e, p) {
        e.YepsID = 'Tjrc7yme5RhWMwEri';
        var s = y.createElement('script');
        e[p] = e[p] || function() {
            (e[p].q = e[p].q || []).push(arguments);
        };
        s.async = true;
        s.src = 'https://widget.yeps.io/yeps.js';
        if (y.head) y.head.appendChild(s);
    })(document, window, 'Yeps');
</script>
<!-- /Yeps {/literal} -->