<?php
cs_var('account_pages', ['account', 'homeschooling', 'wellbeing']);
cs_var('courses', ['homeschooling', 'wellbeing']);
cs_var('course_emails', ['homeschooling' => 'relieffoundation@gmail.com', 'wellbeing' => 'zvmworld@gmail.com']);

function load_account() {
	if (cs_var('local')) {
		cs_var('simulate_auth', true);
		load_amadeus_module('auth');
		return;
	}

	if (cs_var('local'))
		cs_var('db', ['dbname' => 'yieldmore', 'username' => 'root', 'password' => '']);
	else
		cs_var('db', ['dbname' => 'cselian_amYield', 'username' => 'cselian_amYield', 'password' => 'c^iMj5VKV8Ke']);

	load_amadeus_module('auth');
	$auth = cs_var('auth');

	if (isset($_GET['logout']))
		$auth->logOutEverywhere();
	else if (isset($_GET['signin']))
		cs_var('signin_result', auth_signin());
}

function print_course_info() {
	if (cs_var('simulate_auth')) { echo 'Auth Simulated - see load_account'; return; }
	$course = cs_var('node');
	if ($course == 'account') $course = false;

	$auth = cs_var('auth');
	if (!$auth->isLoggedIn()) {
		echo 'Signup &/ Login to ' . ($course ? 'enroll for this course' : 'to see all courses');
		return;
	}

	$courses = auth_courses();
	$nl = cs_var('nl');
	echo '<ol>' . $nl;
	$statuses = cs_var('course_statuses');
	$emails = cs_var('course_emails');
	foreach ($courses as $c => $status) {
		if ($course && $course != $c) continue;
		$more = $status == $statuses['NotApplicable'] ? 'request' : ($status == $statuses['Allowed'] ? 'has access' : 'approval pending');
		if ($more == 'request') $more = sprintf('<a style="display: inline" href="mailto:%s?subject=Enroll for YM Course: %s">%s</a>', $emails[$c], $c, $more);	
		echo sprintf('<li><a style="display: inline" href="%s%s/">%s</a> - %s</li>' . $nl, cs_var('url'), $c, $c, $more);
	}
	echo '</ol>' . $nl;
}

function has_course_access($course) {
	if (cs_var('simulate_auth')) return true;

	$courses = auth_courses();
	return $courses[$course];
}

cs_var('sub-site-width', 'col-lg-8 col-sm-12');
cs_var('sub-site-right-col-width', 'col-lg-4 col-sm-12');

cs_var('folders', [
	'causes' => 'Causes & Movements',
	'explore' => 'Explore Programs, People and Services',
	'healing' => 'Healing and Serenity',
	'people' => 'Welcoming Serious People to grow with us',
	'msa' => 'Independant site of Mother Sri Aurobindo (architects of A Golden World), their devotees and inspired organizations',
	'pact' => 'Education Forum'
	, 'centres' => 'Community Centres'
	, 'enabler' => 'Enabler :: Cut through the clutter!'
	, 'support' => 'Support our Supporters'
	, 'cwc' => 'Condolences with Concern'
	, 'wellbeing' => '1 Year Self Improvement / Absolute Wellbeing Course'
	, 'homeschooling' => 'Self Study Material by the Relief Foundation'
	, 'happyschools' => 'Holistic Evaluation Matrices by the Relief Foundation'
]);

cs_var('copyrights', [
	'enabler' => 'Enabler &copy; 2021 - Mustafa Badsha<br />',
	'wellbeing' => 'Absolute Wellbeing Englightenment Course - &copy; 2021 - Saify Saraiya<br />',
	'homeschooling' => 'Homeschooling Material &copy; 2021 <b>Relief Foundation</b><br />',
	'happyschools' => 'Happy Schools Workshops and Auditing / Consulting &copy; 2021 <b>Relief Foundation</b><br />',
]);

cs_var('themes', [
	'happyschools' => 'tm-vanilla',
]);

cs_var('video-banners', [
	'index' => ['id' => 'PTIqjpkF5Ss', 'title' => 'YieldMore.org Intro - March 2017'],
	'2020' => ['id' => 'MWAK3K7A6_Y', 'title' => 'Join Us - the YieldMore.org 2018 Call to Action'],
	'centres' => ['id' => 'Rx-Ozkfuxgo', 'title' => 'WoW Centres (Joyland) Welcome Note - Feb 2020'],
	'wow' => ['id' => 'SFLytWs4OKc', 'title' => 'Wish On World Themed Poem - Sep 2020'],
	'peer-education' => ['id' => 'qBmZZMwx3js', 'title' => 'Cascade FLS and YieldMore Peer Education Primer'],
	//'wellbeing' => ['id' => 'bancoNCo0kA', 'title' => 'Absolute Wellbeing Online Course'],
]);

//NB: also used in yeps
cs_var('pre-banner-excludes', [
	'enabler',
	'happyschools',
	'cwc',
]);

function before_render() {
	if (cs_var('page_parameters') && array_search('embed', cs_var('page_parameters')) !== false) cs_var('embed', true);
	cs_var('safeFolder', '');
	$copyrights = cs_var('copyrights');
	$themes = cs_var('themes');

	foreach (cs_var('folders') as $folder => $name) {
		$file = cs_var('path') . '/_' . $folder . '/' . cs_var('node') . '.txt';
		if (file_exists($file)) {
			cs_var('dataFile', cs_var('path') . $folder . '/menu.tsv');
			cs_var('folderName', $folder);
			cs_var('folderHeading', $name);
			cs_var('folder', '_' . $folder);
			cs_var('safeFolder', '-' . $folder);
			if (isset($copyrights[$folder])) cs_var('additional_copyright', $copyrights[$folder]);
			if (isset($themes[$folder])) cs_var('theme', $themes[$folder]);
			return;
		}
	}
}

function did_render_page() {
	if (cs_var('folder')) {
		include_once cs_var('path') . '/' . cs_var('folder') . '/index.php';
		return true;
	}
	return false;
}

function pre_banner() {
	if (array_search(cs_var('node'), cs_var('pre-banner-excludes')) !== false || array_search(cs_var('folderName'), cs_var('pre-banner-excludes')) !== false) return;
	//debug_print_backtrace(); die();
	$nl = cs_var('nl');
	//https://support.google.com/gsa/answer/6329153?hl=en
	echo '<!--googleoff: all-->' . $nl;
	echo '<div class="above-banner-container container">' . $nl;
	echo '  <div class="above-banner">' . $nl;
	$url = '<a href="' . cs_var('url') . '%s/">%s</a>';
	$replace = [
		'ideas' => 'IDEAS',
		'causes' => 'CAUSES',
		'support' => 'SUPPORT',
		'peer-education' => 'Peer Education',
		'wish-class' => 'Imran\'s Wish Class',
		'wellbeing' => 'Wellbeing',
		'homeschooling' => 'Homeschooling',
		'wow' => 'WOW (Wish On, World)',
		'enabler' => 'enable',
		'wishes' => 'Wishes Take Wing',
		'country-roads' => 'Country Roads Initiative'
	];
	/*
	$txt = '  YM is at BEGINNINGS - this means that while we do have some %ideas%, none have been adopted (because of us) yet. And while we are trying to champion %causes% with our #DirectDonations idea, no one has (because of us) come forward to %support% anyone YET. Just to set a realistic picture! Yes, we plan to be in business another few hundred years and have a long roadmap. Officially, our journey began on Aug 10th 2013 with Imran.' . $nl;
	*/
	$txt = '  Welcome to YieldMore.org, an organization thats <u>all about promoting others</u>. You may %enabler% / %support% / register existing charities, discuss %wellbeing%, %homeschooling% or %peer-education%, join %wish-class% or have us promote YOUR OWN program / initiative. Dont miss our %country-roads% or <a href="https://imran.yieldmore.org/" target="_blank">Imran\'s Blog</a>.' . $nl;
	foreach ($replace as $key=>$value)
		$txt = str_replace('%' . $key . '%', sprintf($url, $key, $value), $txt);
	echo $txt;
	echo '  </div>' . $nl;
	pre_banner_quote();
	echo '</div>' . $nl;
	echo '<!--googleon: all-->' . $nl;
}

function pre_banner_quote() {
	$nl = cs_var('nl');

	$cols = 'object';
	$items = tsv_to_array(file_get_contents(__DIR__ . '/data/quotes.tsv'), $cols);
	$quote = $items[array_rand($items, 1)];

	if ($id = isset($_GET['quote']) ? $_GET['quote'] : false) {
		foreach ($items as $q) {
			if ($id == $q[$cols->SNo]) {
				$quote = $q;
				break;
			}
		}
	}

	$m_cols = 'object';
	$m_items = tsv_to_array(file_get_contents(__DIR__ . '/data/quotes-macros.tsv'), $m_cols);
	$macro = false;

	foreach ($m_items as $m) { if (trim($m[$m_cols->Key]) == trim($quote[$cols->Macro])) { $macro = $m; break; } }
	$link = $macro ? sprintf('&nbsp;&nbsp;|&nbsp;&nbsp;<a href="%s" target="_blank">%s</a>', $macro[$m_cols->Link], $macro[$m_cols->Text]) : '';
	$more = $quote[$cols->More];

	$html = '<div class="quote-above-banner quote"><u>RANDOM QUOTE</u>: '. $nl;
	$html .= $quote[$cols->What]. $nl;
	$html .= ($more ? ' <a href="#" class="toggle-more">&hellip;</a><div class="mode hidden">' . $more . $link . '</div>' : ''). $nl;
	$html .= '</div>'. $nl;
	echo $html;
}

function page_banner($url = false, $settings = []) {
	$node = isset($settings['node']) ? $settings['node'] : cs_var('node');
	if (!$url && !cs_var('is_sitemap') && !isset($settings['node'])) pre_banner();

	if (!$url && is_array(cs_var('video-banners')) && array_key_exists($node, cs_var('video-banners'))) {
		$video = cs_var('video-banners')[$node];
		echo '<div class="video-bgd"><div class="container"><div class="video-container"><iframe title="' . $video['title'] . '" src="https://www.youtube.com/embed/' . $video['id'] . '?feature=oembed" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div></div>';
		return;
	}

	$imgPath = cs_var('path') . '/assets/pages/%s.jpg';
	$img = sprintf($imgPath, cs_var('node'));
	$banner = cs_var('node');

	if (!file_exists($img) ) {
		if (cs_var('folderName')) {
			$parent = cs_var('currentParentMenuRow');
			$banner = cs_var('folderName');
			$img = sprintf($imgPath, $banner);
			if ($parent) {
				$cols = cs_var('currentMenuColumns');
				$parentImg = sprintf($imgPath, $parent[$cols->Page]);
				if (file_exists($parentImg)) {
					$img = $parentImg;
					$banner = $parent[$cols->Page];
				}
			}
			if (!file_exists($img) ) return false;
		}
		else
		{
			return false;
		}
	}

	$ver = isset($settings['ver']) ? '?ver=' . $settings['ver'] : '';
	if ($url) return cs_var('url') . 'assets/pages/' . $banner . '.jpg' . $ver;
	echo '<img class="banner-img" src="' . cs_var('url') . 'assets/pages/' . $banner . '.jpg' . $ver . '" alt="' . $banner . '" />';
}

function page_about() {
	$seo = seo_about(true);
	if (!$seo) { page_social(); echo '<hr />'; return; }
	if (cs_var('node') != 'index') echo '<h2>' . $seo['name'] . '</h2>';
	$fmt = '  <p>%s%s</p>' . PHP_EOL;
	if ($seo['description']) echo sprintf($fmt, '', '<i>' . $seo['description'] . '</i>');
	if ($seo['keywords']) echo sprintf($fmt, '<b>Keywords: </b>', $seo['keywords']);
	page_social();
	echo '<hr />';
}

function page_social() {
	if (array_search(cs_var('node'), cs_var('pre-banner-excludes')) !== false) return; //TODO: Give mustafa a topic on causes
	echo 'Engage us on Facebook: <a href="javascript: $(\'.fb-links .bold\').siblings().toggle();"><u>See All Groups and Topics</u></a><br />';

	$cols = 'object';
	$links = tsv_to_array(file_get_contents('data/social.tsv'), $cols);

	$nl = cs_var('nl');
	echo '<ul class="fb-links">' . $nl;
	$last_type = ''; $group = ''; $was_one_selected = false;
	foreach ($links as $ix => $link)
	{
		$topic = $link[$cols->Type] == 'Topic';
		if ($link[$cols->Type] == 'Group') $group = 'https://www.facebook.com/groups/' . $link[$cols->Slug] . '/';

		if ($sel = stripos($link[$cols->Pages], cs_var('node')) !== false || ($link[$cols->Section] && ($link[$cols->Section] == cs_var('folderName'))))
			$was_one_selected = true;
		if ($ix == count($links) - 1 && $was_one_selected == false) $sel = true;

		echo sprintf('<li class="%s %s"><a class="fb" href="%s" target="_blank">%s</a></li>' . $nl,
			$sel ? 'bold' : 'hidden', $topic ? 'topic' : 'group', $group . ($topic ? 'post_tags/?post_tag_id=' . $link[$cols->Slug] : ''), $link[$cols->Name]);

		$last_type = $link[$cols->Type];
	}
	echo '</ul>' . $nl;
}

function sub_site_heading()
{
	echo cs_var('folderHeading');
}

function article_or_node_path($default) {
	if (!($article = cs_var('page_parameter1'))) return $default;
	return cs_var('path') . '/_articles/' . cs_var('node') . '/' . cs_var('page_parameter1') . '.txt';
}

function if_articles() {
	$abs_fol = cs_var('path') . '/' . ($fol = '_articles/' . cs_var('node'));
	if (!is_dir($abs_fol)) return;

	$orig_url = cs_var('url');
	$orig_folder = cs_var('folder');
	$orig_folder_name = cs_var('folderName');

	cs_var('url', $orig_url . cs_var('node') . '/');
	cs_var('folder', $fol);
	cs_var('folderName', cs_var('node'));

	echo '<div class="person-articles">Articles by <b>' . str_replace('-', ' ', cs_var('node')) . '</b></div>';
	menu(['folder' => true, 'articles' => true]);
	
	cs_vars([
		'url' => $orig_url,
		'folder' => $orig_folder,
		'folderName' => $orig_folder_name,
	]);
}
?>
