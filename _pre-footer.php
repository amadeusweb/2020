<div id="pre-footer">
	<div class="container">
		<?php if (!cs_var('local') && uses('tlk')) { ?>
		<hr />
			<div style="text-align: center;"><a href="https://tlk.io/yieldmore" target="_blank">Talk to us on TLK.IO</a></div>
			<div id="tlkio" data-channel="yieldmore" data-theme="theme--day" style="width:100%;height:400px;"></div>
			<script async src="https://tlk.io/embed.js" type="text/javascript"></script>
		<?php } ?>
	</div>
</div>
