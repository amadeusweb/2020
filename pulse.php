<section id="pulse">
  <div class="container">
    <?php page_about(); ?>
    <div class="row">
    
      <div class="<?php echo cs_var('sub-site-width'); ?>">
<?php 
if (cs_var('node') != 'index') echo wpautop('In time, this page will embed content from Facebook and other social media and tell you whats trending on our forum. Until then, here are our facebook links.');
$social = [
    ['type' => 'fb', 'name' => 'FB page of YieldMore.org', 'url' => 'https://www.facebook.com/YieldMoreOrg/'],
    ['type' => 'fb', 'name' => 'Main FB group of YieldMore.org', 'url' => 'https://www.facebook.com/groups/YieldMore/'],
    /*
    ['type' => 'fb', 'name' => 'Explore the Conscious Marketplace', 'url' => 'https://www.facebook.com/groups/ExploreYM/'],
    ['type' => 'fb', 'name' => 'Curation, Learning, Creativity and Education', 'url' => 'https://www.facebook.com/groups/LearningYM/'],
    ['type' => 'fb', 'name' => 'Health and Healing Channel and Forum', 'url' => 'https://www.facebook.com/groups/HealingYM/'],
    ['type' => 'fb', 'name' => 'Share Thoughts and Discuss Topics', 'url' => 'https://www.facebook.com/groups/ExpressYM/'],
    ['type' => 'fb', 'name' => 'Causes and Movements', 'url' => 'https://www.facebook.com/groups/causesandmovements/'],
    */
    ['type' => 'fb', 'name' => 'Imran\'s FB YM Account', 'url' => 'https://www.facebook.com/imran.yieldmore/'],
    ['type' => 'fb', 'name' => 'Imran\'s FB Page', 'url' => 'https://www.facebook.com/ImranAtYM'],
    ['type' => 'fb', 'name' => 'In Memoriam YM Group', 'url' => 'https://www.facebook.com/imran.yieldmore/'],

    ['type' => 'fb', 'name' => 'Expression and Writing group', 'url' => 'https://www.facebook.com/groups/WritingYM/'],
];
    echo '<ul>' . cs_var('nl');
    foreach ($social as $link)
      echo sprintf('<li><a class="%s" href="%s" target="_blank">%s</a></li>' . cs_var('nl'), $link['type'], $link['url'], $link['name']);
    echo '</ul>' . cs_var('nl');

//print_hashtags();

function print_hashtags() {
	$linkCols = 'object';
	$links = tsv_to_array(file_get_contents('hashtags.tsv'), $linkCols);
	echo '<hr /><h2>Hashtags (Facebook)</h2>';
	foreach ($links as $link) 
		echo '<a href="https://www.facebook.com/hashtag/' . $link[$linkCols->Tag] . '" target="_blank" class="silent-external-link" title="' . $link[$linkCols->Area] . ': ' . $link[$linkCols->Description] . '">#' . $link[$linkCols->Tag] . '</a><br />';
}
?>
      </div>
    
      <div class="<?php echo cs_var('sub-site-right-col-width'); ?>">
        <?php facebook_widget('yieldmore.org'); ?>
      </div>
    </div>
  </div>
 </section>
