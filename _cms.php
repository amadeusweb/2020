<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

include_once '../../amadeus/framework/core.php';
include_once '_functions.php';
cs_var('local', $local = $_SERVER['HTTP_HOST'] ==='localhost');

bootstrap(array_merge(include_once('_config.php') , array(
	'name' => 'YieldMore.org 2020',
	'byline' => 'All LOVE to the World Family (Jagat Kula Ki Prem)',
	'logo' => true,
	'safeName' => 'yieldmore',

	'version' => [
		'id' => '004',
		'date' => '19 Feb 2021',
	],

	'support_page_parameters' => true, //NB: For wellbeing
	'uses' => 'default-bgds, tlk', //for tm-vanilla

	'styles' => ['styles'],
	'scripts' => ['../conversations/conversations', 'courses', 'main', 'lyrics'],

	'url' => $local ? 'http://localhost/yieldmore/2020/' : 'https://2020.yieldmore.org/',
	'path' => __DIR__,
)));

if ( array_search(cs_var('node'), cs_var('account_pages')) !== -1 ) load_account();

render();
?>
