$(document).ready(function() {
	$('.protected .buy').click(loadProtected);

	function loadProtected() {
		var url = '../../_serve.php?file=' + $(this).data('course') + '/' + $(this).data('subnode');
		$.ajax(url).done(function (txt){
			$('.protected').html('');
			$('.protected').append('<hr />').append(txt);
		});
	}

	if (window.access) $('.protected .buy').trigger('click');
});
