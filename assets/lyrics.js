$(document).ready(function() {
	$('.lyrics-versions .toggle-version').click(function() {
		var input = $('input', $(this));
		var divs = $('div.' + input.data('version'));
		if (input.is(':checked')) divs.show(); else divs.hide();
	});
	$.each($('.lyrics-verse'), function(ix, el) {
		$(this).prepend('<h3 class="verse">Verse: ' + $(this).data('index') + '</h3>');
	});
	$.each($('.time-sync'), function(ix, el) {
		var lang = $(this).data('file') ? $(this).data('file') : 'default';
		var time = $(this).data('time');
		$(this).prepend('<a class="goto-time" data-file="' + lang + '" data-time="' + time + '">Time (secs): ' + time + ' (' + lang + ')</a>');
	});
	$('.goto-time').click(function() {
		var player = $('.player-' + $(this).data('file'));
		$.each(player.siblings('.drive-mp3'), function() {
			this.pause();
		});
		player[0].currentTime = parseFloat($(this).data('time'));
		player[0].play();
	});
});
