<style type="text/css">
<?php echo replace_vars(file_get_contents(__DIR__ . '/assets/styles.css')); ?>
</style>
<section id="<?php echo cs_var('node'); ?>">
	<?php page_banner(false, ['ver' => 2]); ?>
	<div class="container">
		<?php //page_about() ?>
		<div class="section-content">
			<!--
			<div class="section-menu">
				<?php //include_once('_menu.php') ?>
			</div>
			<hr />
			<h2><?php sub_site_heading(); ?></h2>
			-->
			<?php echo wpautop(file_get_contents(__DIR__ . '/' . (cs_var('page_parameter1') ? cs_var('page_parameter1') : cs_var('node')) . '.txt'));?>
		</div>
	</div>
</section>
