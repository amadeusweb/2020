<h2>MIND MAP 5: EXCHANGE</h2>

#Description: TODO (I AM SINCERE)

Exchange brings business of Easy-ness in busy-ness. An activity to provide what people appropriately as per need - (demand and supply when and to who needs it). Not giving to people who do not need, or dumping what you have extra.

INTEGRATE, BOND, RELATE, SHARE - 'Let the fragmented become whole'. Exchange to deeply Connect and Network with People for Mutual Support and Growth.

In MIND MAP Five, there's no place for comparison, competition or confrontation, Matrix five is all about compliance, complimenting and cooperating for mutual growth and progress.

The Diagram

A. What I need from the other, is it mutual?  - (I Win - Others Win). "Unless I exchange the price for the value I receive or give, its benefit cannot get capitalised" - it is like "Throwing Pearls Before the Swine". Relationship progresses only if it is uncompromisingly in mutual interest and follows the policy of 'WIN-WIN or No Deal'.

"kabeera, aap thagaye, aur na thagye koi, aap thage sukh upje, aur thage dukh hoi".

B) In a LOSE/WIN Situation, we lose to let others win due to emotional need, yet later we harbour regret, affecting our relationships. (Heartfully share your blessings - forgive - inform eg., Jayawardene Japan - negotiate and proceed to A).

C) In a WIN /LOSE situation, we dominate and win; the other loses, the heart of the other is against us, we suffer guilt. (Soulfully seek forgiveness - make amends - compensate and proceed to A).

D) A disastrous destiny - both B & C if not resolved would lead to a Lose / LOSE situation harbouring disregard, despise, feelings for revenge and toxic built-up leading to the worst kind of disease in the body. (Seek refuge and protection from the Universal Force by forgiving and asking to be forgiven)

Integrate, not Unite. Unity is evil, for unity is always against something. A group united against another. Evil provokes in us to be selfish - my family only, my neighbourhood only, my flat association, (visitors not allowed to park their car inside) cultivate 'atithi devo bhava' spirit ). region, race, rank, business, profession, gender..

Integrate as 'Vasudhaiva Kutumbakam', as 'Yaadhum Oore Yaavarum Kelir', as Ubuntu, then our body will be healthy, no cancer cells, blockages, constriction of arteries or blood vessels. So no heart attack, paralysis or arthritis.
