<?php
$file = ''; $buffer = []; $heading = '';
$nl = cs_var('nl');

$lines = explode("\r\n", file_get_contents(__DIR__ . '/_syllabus.txt'));
$file_count = 0;

echo 'Read ' . count($lines) . ' lines<br />' . $nl;

foreach ($lines as $line) {
	$line = trim($line);
	if (startsWith($line, '#') && !startsWith($line, '#Description:')) {
		if (count($buffer)) {
			echo 'Wrote ' . $file . ' with '. count($buffer) . ' lines<br />' . $nl;
			file_put_contents(__DIR__ . '/' . $file . '.txt', '<h2>' . $heading . '</h2>' . $nl . $nl . implode($nl, $buffer));
			$buffer = [];
		}
		$heading = substr($line, 1);
		if ($no_count = startsWith($heading, '_') || startsWith($heading, '#')) $heading = substr($heading, 1);
		$file = ($no_count ? '' : ++$file_count . '-') . urlize(explode(':', $heading)[0]);
	} else {
		$buffer[] = simplify_encoding($line);
	}
}
echo 'Wrote ' . $file_count . ' files<br />' . $nl;

?>
