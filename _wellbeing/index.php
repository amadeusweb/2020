<section id="<?php echo cs_var('node'); ?>">
	<?php page_banner(); ?>
	<div class="container">
		<?php page_about() ?>
		<div class="row">
			<div class="<?php echo cs_var('sub-site-width'); ?>">
			<?php if (cs_var('page_parameter1') == 'export') include '_export.php'; else echo wpautop(file_get_contents(__DIR__ . '/' . (cs_var('page_parameter1') ? cs_var('page_parameter1') : cs_var('node')) . '.txt'));?>
			</div>
		
			<div class="<?php echo cs_var('sub-site-right-col-width')?> sub-site">
				<?php include __DIR__ . '/../account.php'; ?>
				<hr />
				<p class="links-inline"><b>NOTICE</b>: <?php echo replace_vars('This course content is given with the understanding that the beneficiary will donate Rs 7,500 or $150 to the <a href="%url%wellbeing/trainers/">Trainer</a> of this, the "Absolute Wellbeing Course" designed by Saify Saraiya.'); ?></p>
				<hr />
				<div class="related-links"><?php sub_site_heading(); ?></div>
				<?php menu(['folder' => true, 'folder_in_url' => true]) ?>
			</div>
		</div>
	</div>
</section>
<script>window.access = <?php echo has_course_access(cs_var('node')) ? 'true' : 'false'; ?></script>