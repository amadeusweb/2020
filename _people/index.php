<?php if (file_exists(__DIR__ . '/' . cs_var('node') . '.php')) { include cs_var('node') . '.php'; return; } ?>
<section id="<?php echo cs_var('node'); ?>">
	<?php page_banner(); ?>
	<div class="container">
		<?php page_about() ?>
		<div class="row">
			<div class="<?php echo cs_var('sub-site-width'); ?>">
			<?php
			$standalones = [
				'csf2021' => [
					'name' => 'Chennai Storytelling Festival 2021',
					'text' => 'Lorem ipsum dolor sit amet consectetuer urna sit quis cursus enim. Quis velit lacinia sapien elit elit tristique consequat Aliquam Cras dictum. 

Consectetuer vitae interdum in quam dui adipiscing netus molestie auctor ligula. Semper mauris Sed orci tempus suscipit sociis dui vitae facilisis tortor. Senectus ridiculus vitae justo orci mauris nibh dui fringilla dolor enim. Vestibulum tellus suscipit mus Nulla condimentum.

Curabitur mus tellus natoque eros Donec Nullam nibh molestie ante Quisque. Nibh laoreet metus interdum at orci et wisi velit.',
					'flyers' => [
						'yieldmore',
						'yieldless',
					],
				],
			];
			$is_standalone = array_search(cs_var('node'), ['csf2021']) !== false;
			$standalone = $is_standalone ? $standalones[cs_var('node')] : false;
			$flyers = $is_standalone ? $standalone['flyers'] : [];
			//print_r($flyers); die();
			if ($is_standalone) include_once(__DIR__ . '/' . cs_var('node') . '.php');
			else echo wpautop(file_get_contents(__DIR__ . '/' . cs_var('node') . '.txt'));
			?>
			</div>
		
			<div class="<?php echo cs_var('sub-site-right-col-width')?> sub-site">
			<?php if (!$is_standalone) { ?>
				<div class="related-links"><?php sub_site_heading(); ?></div>
				<?php menu(['folder' => true]) ?><?php } else { ?>
				<div class="related-links"><?php echo $standalone['name']; ?></div>
				Media Kit:
				<ol><?php foreach($flyers as $f) {?>
					<li><?php echo sprintf('<a href="%s%s/%s/">Flyer by %s</a>', cs_var('url'), cs_var('node'), $f, $f); ?></li><?php } ?>
				</ol>
				<?php echo wpautop($standalone['text']); } ?>
			</div>
		</div>
	</div>
</section>
