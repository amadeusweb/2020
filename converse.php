<div class="container">
<?php include "conversations/conversations.php";

page_social();

echo '<h3><a href="https://yieldmore.org/converse/" target="_blank">Amadeus Converse</a></h3>' . cs_var('nl');
echo '<p>Let visitors <u title="on social media / instant messaging / email">engage you</u> with <u title="relevant to your projects, activities, events articles etc">prebuilt crafted messages</u>. Know More with our <a href="' . cs_var('url') . 'converse/faqs/" target="_blank">FAQs</a>. <a href="tel:+91-9841223313">Contact Imran</a> to be listed here.</p><hr />';

if (cs_var('page_parameter1') == 'faqs') {
	echo '<a href="../">Back to Amadeus Converse (Keep Talking)</a>';
	$cols = 'object';
	$faqs = tsv_to_array(file_get_contents('conversations/faqs.tsv'), $cols);
	$fmt = '<div style="border: 2px solid #fcc; margin-bottom: 20px; padding: 6px;"><h3 onclick="$(this).next().toggle();">%s</h3><div style="margin-top: 12px">%s</div></div>';
	foreach ($faqs as $f) echo sprintf($fmt, $f[$cols->Question], $f[$cols->Answer] . ($f[$cols->Notes] ? '<div style="margin-top: 12px; font-size: 85%">' . $f[$cols->Notes] . '</div>' : ''));
} else {
	renderConversations([
		'id' => cs_var('page_parameter1'),
		'selected' => cs_var('page_parameter2'),
		'url' => cs_var('url') . 'converse/%s',
		'dataUrl' => cs_var('url') . 'conversations/',
	]);
}
?>
</div>
