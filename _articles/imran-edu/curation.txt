Description: Underscores the importance of books, movies and music in shaping childrens thoughts and how these can trigger learning activities.

We, the team at YieldMore.org postulate that all conventional classroom lessons taught can be triggered by a story or song. When students are totally engaged in the narrative being evolved, and left to derive their own spinoff stories from it, we call this Immersive Curation Based Education.

This sort of learning does not have to be restricted to classrooms only.

Take for example the kindergarten student Amy who is being taught nursery rhymes.

Does she know how humpty dumpty's wall was built? What would have happened if it had been made of cardboard? Could it have been made of delectable sweets like the witch did in the story of Hansel and Gretel?

Do we know why Jack and Jill went up the hill? What all uses could the water have had? Was it for the radiator of Jack's father Peter's tractor? Do we know if it was to a spring or well, why there wasn't one down the hill? Do we know what water divining means? How does one find water in a desert? Where on earth is there the most rainfall? Apart from rain, how do rivers get their water?

We underscore the need for derivative stories and mixins of characters from different stories written by the children themselves. Each child should be encouraged to talk of their "sweaty toothed madmen" as often as they can&hellip; like Todd here:

<a class="yt" href="https://www.youtube.com/watch?v=gQU3EphIpMY&list=PLsuI89eMBnMHeQvgPaHV23_t3xIUmbkKm&index=5" target="_blank">Todd Anderson Speaks In Front Of The Class</a>.

Imran, August 2019