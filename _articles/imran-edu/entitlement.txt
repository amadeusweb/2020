<h2>Entitlement</h2>

<h3>Problem Statement</h3>

In our grandparents generation, people were preoccupied with the War Effort and mass communication was limited or one way.

In our parents generation, there was freedoms to work and think and families to an extent started becoming nuclear. There was a burst in reading and learning as the world became smaller. Their's was the generation of the 60s to 70s and most things were won by the sweat of their brow.

Our generation grew up in comparable luxury with well meaning parents spoiling us in a sense. Left to our own, we got used to everything going our way and so felt "entitled" to certain basic things and certain pleasures.

It was natural for over protective parents to want to give their children everything, little did they realise that by doing so, they were not preparing them adequately for the cold harsh world.

Now we're in the 2020s with most of our youth and even the younger parents feeling they are entitled to this or that.

Truth is, there is much poverty in the world, and uncertainty. People lose jobs, people fall ill, breadwinners die, skills become obsolete, we have mid life crises, we glorify money and comforts and become soft.

These should be as alarm bells, telling us to pursue something more wholesome, not fleeting. But here we are in the Entitlement trap, watching each day as our children become more and more petulant and demanding, with neither parent having the time to really understand their psyche or extricate them from this trap.

Poverty, lack of access to food, water, education, human trafficking, the refugee crisis, wars and overpopulation all take their toll on society and if our children are not made sensitive to these, how will they understand / face / combat these?

<hr />

<h3>A Teacher's Perspective</h3>

In my class I have 20 children. Some rich, some poor, some given freedoms, some stifled. I've seen enough of humanity to know that nothing is black and white.

It's a little rare, but sometimes I see wealthy kids taught to value other's and money/possessions. Just fortunate to have grounded elders in their families I suppose.

Some don't realize the value of money until they have seen poverty firsthand. Some poor children are taught to never let that seem a handicap, instead they are rich in the coin of spirit.

I only wish that today's children could break down the barriers between their classes, statuses and poises and given a chance to know intimately the beginnings of each other's parents lives amd their own, for then, the riddle of this world would seem a little less.

Dear parents, do introspect within and among yourselves, find out what Entitlement means to you, what you really want for ypur kids and open them ip to thw joy of seeing the world in Evolving Sunlight.