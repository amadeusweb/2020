<div class="container">
  <?php page_about() ?>
  <div class="row">
	<div class="col-md-6 col-xs-12">
<?php
$pg = cs_var('node');

cs_var('is_sitemap', true);
cs_var('node', 'index');
page_banner();
menu();

foreach (cs_var('folders') as $fol => $name)
{
	echo '</div><div class="col-md-6 col-xs-12">';
	cs_var('folder', '_' . $fol);
	cs_var('node', $fol);
	page_banner();
	menu(['folder' => true]);
}

cs_var('node', $pg);
?>
	</div>
  </div>

<h1>In Banners</h1>
<?php
//TODO: read SEO description
$pages = get_image_folder('assets/pages', false);
//print_r($pages);
$fmt = '<a href="%s%s/" target="_blank" class="silent-external-link"><img src="../assets/pages/%s.jpg" class="img-fluid" /></a><br /><br />';
$suffixes = ['-sq', '-wa'];
foreach ($pages as $page) {
	$sm = false;
	foreach ($suffixes as $suffix) if (endsWith($page, $suffix)) $sm = true;
	if ($sm) continue;
	echo sprintf($fmt, cs_var('url'), $page, $page, $page);
}
?>
</div>
