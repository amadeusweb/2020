<?php if (isset($_GET['lorem'])) {
	include cs_var('app-fol') . 'themes/' . cs_var('theme') . '/home.php';
	return;
}?>

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex flex-column justify-content-center align-items-center">
    <div class="container" data-aos="fade-in">
      <h1>Welcome to <?php echo cs_var('name'); ?></h1>
      <h2 class="inline"><?php echo cs_sub_var('rich_footer', 'description'); ?></h2>
      <!--<div class="inline d-flex align-items-center">
        <i class="bx bxs-right-arrow-alt get-started-icon"></i>
        <a href="../register" class="btn-get-started scrollto">Register Now</a>
      </div>-->
    </div>
  </section><!-- End Hero -->

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us">
      <div class="container">

        <div class="row">
          <div class="col-xl-4 col-lg-5" data-aos="fade-up">
            <div class="content">
              <h3>What is YieldMore.org!</h3>
              <p>
                Join our <a href="../pulse/">community</a> that <a href="../ideas/">tries to help</a>.<br /><br />

                <!-- Our main channels are for Joyful Learning, Integral Healing and Involved Helping. Talk to us today! -->
                Explore <a href="../https://www.youtube.com/watch?v=pmO6dEjD9CE" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true">our Conscious Marketplace</a> which <a href="../explore/">features programs and services</a>.<br /><br />

                Espouse a cause that touches you!
              </p>
              <div class="text-center">
                <a href="../causes/" class="more-btn">Causes & Movements</a>
                <!-- <a href="../contact" class="more-btn">Learn More <i class="bx bx-chevron-right"></i></a> -->
              </div>
            </div>
          </div>
          <div class="col-xl-8 col-lg-7 d-flex">
            <div class="icon-boxes d-flex flex-column justify-content-center">
              <div class="row">
                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="iconym iconym-causes"></i>
                    <h4><a href="../causes/">Causes</a></h4>
                    <p>Enjoin a Movement, Promote YOUR Causes. Find places to volunteer your time.</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="iconym iconym-wow"></i>
                    <h4><a href="../centres/">Wishing Time</a></h4>
                    <p>Wish On, World (the WoW Program) builds support networks and aids communities.</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="iconym iconym-explore"></i>
                    <h4><a href="../explore/">Explore</a></h4>
                    <p>Find enabling programs and services from our Conscious Marketplace</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Why Us Section -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about section-bg">
      <?php page_banner(); ?>
      <div class="container">
        <?php page_about(); ?>

        <div class="row">
          <div class="col-xl-5 col-lg-6 video-box d-flex justify-content-center align-items-stretch" data-aos="fade-right">
            <a href="../https://www.youtube.com/watch?v=PTIqjpkF5Ss" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"><span class="sr-only">yieldmore youtube video</span></a>
          </div>

          <div class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
            <h4 data-aos="fade-up">About us</h4>
            <h3 data-aos="fade-up">YieldMore.org is a link between people, organizations and movements.</h3>
            <p data-aos="fade-up">We showcase ideas, articles and works that are worth sharing. We do this by inspiring people to LEARN, HEAL and SHARE, thereby improving the quality of their lives. When then ask them to express themselves and help their fellow beings.</p>

            <div class="icon-box" data-aos="fade-up">
              <div class="icon"><i class="iconym iconym-learning"></i></div>
              <h4 class="title"><a href="../schools/">Joyful Learning</a></h4>
              <p class="description"><a href="../https://www.facebook.com/groups/LearningYM/">Curation, Learning, Creativity and Education</a> are our main focus on the learn channel. See our Wishing Tree initiative in schools.</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="iconym iconym-healing"></i></div>
              <h4 class="title"><a href="../healing/">Integral Healing</a></h4>
              <p class="description">From our <a href="../https://2019.yieldmore.org/little-things/" target="_blank">Little Things</a> initiative to our <a href="../gaia/">Healing Tips</a> and <a href="../affirmations/">Affirmations Programs</a>, were ALL FOR <a href="../https://www.facebook.com/groups/HealingYM/" target="_blank">promoting new age HEALING techniques</a>.</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
              <div class="icon"><i class="iconym iconym-helping"></i></div>
              <h4 class="title"><a href="../causes/">Involved Helping</a></h4>
              <p class="description">We organize Charities (NGOs) by the <a href="../causes/">Causes & Movements</a> they serve and offer our <a href="../https://panishq.com/services/" target="_blank">media and promotion services to them</a>.
            </div>

          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= We-Promote Section ======= -->
    <section id="clients" class="clients">
      <div class="container" data-aos="fade-up">

        <div class="section-title" data-aos="fade-up">
          <h2>We Support</h2>
          <p>A few organizations that we support</p>
        </div>

        <div class="owl-carousel clients-carousel">
          <a href="https://satyamyogatrust.net/"><img src="../assets/we-promote/satyamyogatrust.jpg" alt="satyamyogatrust" /></a>
          <a href="https://brainsync.com/"><img src="../assets/we-promote/brainsync.png" alt="BrainSync" /></a>
          <a href="https://noetic.org/"><img src="../assets/we-promote/noetic.png" alt="IONS" /></a>
          <!-- <a href="https:///"><img src="assets/we-promote/.png" alt="" /></a> -->
        </div>

      </div>
    </section><!-- End We-Promote Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>What We Do</h2>
          <p>Now in May 2020, we are just beginning operations. Here are some of the things we are trying to work on.</p>
        </div>

        <div class="row">
          <div class="col-lg-4 col-md-6" data-aos="fade-up">
            <div class="icon-box">
              <div class="icon"><i class="icofont-computer"></i></div>
              <h4 class="title"><a href="https://panishq.com/" target="_blank">Web Promotion</a></h4>
              <p class="description">If you are an educator, healer or charity, we can promote / offer media and web services</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class="icofont-chart-bar-graph"></i></div>
              <h4 class="title"><a href="../people/">Individual Growth</a></h4>
              <p class="description">Find life coaches and healers who can help overcome challenges and enjoy living.</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class="icofont-earth"></i></div>
              <h4 class="title"><a href="../ideas/">Ideas for Action</a></h4>
              <p class="description">Connect with other people passionate about helping the world become a better place.</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="icofont-image"></i></div>
              <h4 class="title"><a href="../people/#heroes" target="_blank">Marvellous Heroes</a></h4>
              <p class="description">Know someone inspiring? Promote their stellar work on our Heroes network.</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon"><i class="icofont-settings"></i></div>
              <h4 class="title"><a href="../sites/joyland/flavours" target="_blank">Community Centers</a></h4>
              <p class="description">Joyland, Mindful Work and Wishing Tree are flavours of our Community Center idea.</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="500">
            <div class="icon-box">
              <div class="icon"><i class="icofont-tasks-alt"></i></div>
              <h4 class="title"><a href="https://2019.yieldmore.org/little-things/" target="_blank">Little Things</a></h4>
              <p class="description">Do something small each day and watch amazed as your wishes start to take shape.</p>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Values Section ======= -->
    <section id="values" class="values">
      <div class="container">

        <div class="row">
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card" style="background-image: url(../assets/home/our-mission.jpg);">
              <div class="card-body">
                <h5 class="card-title"><a href="../about/#mission">Our Mission</a></h5>
                <p class="card-text"><a href="../about/#mission">We want to help people heal, grow and thrive, learning to "<u title="Bill and Ted">be excellent to each other</u>" and to nature. &hellip; </a></p>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="100">
            <div class="card" style="background-image: url(../assets/home/our-plan.jpg);">
              <div class="card-body">
                <h5 class="card-title"><a href="../about/#plan">Our Plan</a></h5>
                <p class="card-text"><a href="../about/#plan">We love problem solving and finding the best people to help you achieve your life goals. &hellip; </a></p>
              </div>
            </div>

          </div>
          <div class="col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up" data-aos-delay="200">
            <div class="card" style="background-image: url(../assets/home/our-vision.jpg);">
              <div class="card-body">
                <h5 class="card-title"><a href="../about/#vision">Our Vision</a></h5>
                <p class="card-text"><a href="../about/#vision">We believe there's enough goodness around but it needs to spread like wildfire. We are here to catalyse that. &hellip; </a></p>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up" data-aos-delay="300">
            <div class="card" style="background-image: url(../assets/home/our-care.jpg);">
              <div class="card-body">
                <h5 class="card-title"><a href="../about/#care">Our Care</a></h5>
                <p class="card-text"><a href="../about/#care">Ours is a </a><a href="../https://en.wikipedia.org/wiki/Conscious_business" target="_blank">Conscious Business</a><a href="../about/#care">, thats why we take time to understand everyone in order to serve to the best. &hellip; </a></p>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Values Section -->

    <!-- ======= Wellwishers Section ======= -->
    <section id="testimonials" class="testimonials">
      <div class="container" data-aos="fade-up">

        <div class="owl-carousel testimonials-carousel">
<?php
$is_home = true;
include_once 'appreciation.php';
foreach ($appreciation as $person) {?>

          <div class="testimonial-item">
            <img src="../assets/wellwishers/<?php echo $person['img']; ?>" class="testimonial-img" alt="">
            <h3><?php echo $person['name']; ?></h3>
            <h4><?php echo $person['role']; ?></h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              <?php echo isset($person['short']) ? $person['short'] : $person['writeup']; ?>
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div><?php } ?>

        </div>

      </div>
    </section><!-- End Wellwishers Section -->

<?php if (false) { ?>
    <!-- ======= Interests Section ======= -->
    <section id="portfolio" class="Portfolio">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up">Links</h2>
          <p data-aos="fade-up">These are our FB Groups, interests, ideas and initiatives. Click the link to join the discussion or head over to the <a href="../pulse/">pulse page</a> to know more.</p>
        </div>

		<?php include "pulse.php"; ?>

<?php if (false) { ?>
        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-Facebook">Facebook</li>
              <li data-filter=".filter-Site">Site</li>
              <li data-filter=".filter-Youtube">Youtube</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

<?php
$linkCols = 'object';
$links = tsv_to_array(file_get_contents('links.tsv'), $linkCols);
foreach ($links as $link) {?>
          <div class="col-lg-4 col-md-6 portfolio-item filter-<?php echo $link[$linkCols->area]; ?>">
            <img src="assets/links/<?php echo $link[$linkCols->image] ? $link[$linkCols->image] : 'link.jpg'; ?>" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4><?php echo $link[$linkCols->name]; ?></h4>
              <p><?php echo $link[$linkCols->area]; ?></p>
              <a href="<?php echo $link[$linkCols->link]; ?>" target="_blank" class="venobox preview-link silent-external-link" title="<?php echo $link[$linkCols->name]; ?>"><i class="bx bx-plus"></i></a>
              <a href="../pulse#<?php echo $link[$linkCols->slug]; ?>" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
<?php } }?>

        </div>

      </div>
    </section><!-- End Interests Section -->
<?php } ?>

<?php if (false) { ?>
    <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up">Team</h2>
          <p data-aos="fade-up">Led by Imran with some rock solid help and Great People to work with, our team requires your support and pomotion to take our ideas to the next level.</p>
        </div>

        <div class="row">

<?php
$personCols = 'object';
$team = tsv_to_array(file_get_contents('team.tsv'), $personCols);
foreach ($team as $person) {?>
          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="member">
              <div class="member-img">
                <img src="assets/team/<?php echo strtolower($person[$personCols->name]); ?>.jpg" class="img-fluid" alt="<?php echo $person[$personCols->name]; ?>">
                <div class="social">
                  <?php if ($person[$personCols->twitter]) {?><a href="https://twitter.com/<?php echo $person[$personCols->twitter]; ?>" target="_blank" class="silent-external-link"><i class="icofont-twitter"></i></a><?php } ?>
                  <?php if ($person[$personCols->facebook]) {?><a href="https://facebook.com/<?php echo $person[$personCols->facebook]; ?>" target="_blank" class="silent-external-link"><i class="icofont-facebook"></i></a><?php } ?>
                  <?php if ($person[$personCols->instagram]) {?><a href="https://www.instagram.com/<?php echo $person[$personCols->instagram]; ?>" target="_blank" class="silent-external-link"><i class="icofont-instagram"></i></a><?php } ?>
                  <?php if ($person[$personCols->linkedin]) {?><a href="https://www.linkedin.com/<?php echo $person[$personCols->linkedin]; ?>" target="_blank" class="silent-external-link"><i class="icofont-linkedin"></i></a><?php } ?>
                </div>
              </div>
              <div class="member-info">
                <h4><?php echo $person[$personCols->name]; ?></h4>
                <span><?php echo $person[$personCols->role]; ?></span>
              </div>
            </div>
          </div>
<?php } ?>

        </div>

      </div>
    </section><!-- End Team Section -->
<?php } ?>
