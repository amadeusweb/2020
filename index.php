  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex flex-column justify-content-center align-items-center" style="background: url(assets/home/hero-bg.jpg) center center; background-size: cover;">
    <div class="container" data-aos="fade-in">
      <h1>Welcome to <?php echo cs_var('name'); ?></h1>
      <h2 class="inline"><?php echo cs_var('byline'); ?></h2>
      <!--<div class="inline d-flex align-items-center">
        <i class="bx bxs-right-arrow-alt get-started-icon"></i>
        <a href="register" class="btn-get-started scrollto">Register Now</a>
      </div>-->
    </div>
  </section><!-- End Hero -->

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us">
      <div class="container">

        <div class="row">
          <div class="col-xl-4 col-lg-5" data-aos="fade-up">
            <div class="content">
              <h3>What is YieldMore.org!</h3>
              <p>
                Join the <a href="converse/">conversation</a>.<br /><br />

                <!-- Our main channels are for Joyful Learning, Integral Healing and Involved Helping. Talk to us today! -->
                Explore <a href="https://www.youtube.com/watch?v=pmO6dEjD9CE" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true">our Conscious Marketplace</a> which <a href="explore/">features programs and services</a>.<br /><br />

                Espouse a cause that touches you!
              </p>
              <div class="text-center">
                <a href="support/" class="more-btn">Support a Charity</a>
                <!-- <a href="contact" class="more-btn">Learn More <i class="bx bx-chevron-right"></i></a> -->
              </div>
            </div>
          </div>
          <div class="col-xl-8 col-lg-7 d-flex">
            <div class="icon-boxes d-flex flex-column justify-content-center">
              <div class="row">
                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="iconym iconym-awe"></i>
                    <h4><a href="wellbeing/">Wellbeing</a></h4>
                    <p>Live a life with Absolute Wellbeing Enlightenment, Join us Weekly.</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="iconym iconym-pact"></i>
                    <h4><a href="pact/">PACT</a></h4>
                    <p>Be the first to join our education forum and help shape our children's destiny.</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="iconym iconym-enabler"></i>
                    <h4><a href="explore/">Enabler</a></h4>
                    <p>Support a Charity Initiative that ENABLES people. Cut through the clutter.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Why Us Section -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about section-bg">
      <?php page_banner(); ?>
      <div class="container">
        <?php page_about(); ?>

        <div>
            <div class="tab-links">
                <span>Our History - us in:</span>
                <a href="2020/">2020</a>
                <a href="https://2019.yieldmore.org/" target="_blank">2019</a>
                <a href="https://legacy.yieldmore.org/" target="_blank">2018</a>
                <a href="https://legacy.yieldmore.org/about/" target="_blank">2017</a>
                <a href="https://legacy.yieldmore.org/works/" target="_blank">2015</a>
                <a href="https://cselian.com/blog/category/yield/" target="_blank">2013</a>
                <a href="https://biblios.cselian.com/" target="_blank">2011</a>
            </div>
            <hr />
            <div class="row services">
                <div class="col-lg-6 col-sm-12"><div class="icon-box"><?php echo wpautop(file_get_contents('../imran/_excerpts/catalyst.txt')); ?></div></div>
                <div class="col-lg-6 col-sm-12"><div class="icon-box"><?php echo wpautop(file_get_contents('../imran/_excerpts/wishes.txt')); ?></div></div>
                <div class="col-lg-6 col-sm-12"><div class="icon-box"><?php echo wpautop(file_get_contents('../imran/_excerpts/joyland-welcome.txt')); ?></div></div>
                <div class="col-lg-6 col-sm-12"><div class="icon-box"><?php echo wpautop(file_get_contents('../imran/_excerpts/cry-liberty.txt')); ?></div></div>
            </div>
        </div>
        <div>
            <h3><a href="centres/">Help us to Start a Centre</a></h3>
            <?php page_banner(false, ['node' => 'centres']); ?>
        </div>

      </div>
    </section><!-- End About Section -->

