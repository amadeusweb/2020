<?php
function renderConversations($s) {
	$single = $s['id'] ? $s['id'] : false;
	$selected = $s['selected'] ? $s['selected'] : false;
	$fols = $single ? [$single] : scandir(__DIR__);
	foreach ($fols as $f) {
		if ($f == '.' || $f == '..' || !is_dir($fol = __DIR__ . '/' . $f)) continue;
		echo sprintf('<a href="%s"><b>%s</b></a>: ', sprintf($s['url'], $f) . '/', $f);
		$files = scandir($fol);
		foreach ($files as $fi) {
			if ($fi == '.' || $fi == '..') continue;
			$name = str_replace('.json', '', $fi);
			echo sprintf('<a class="silent-external-link repo%s" href="%s" target="_blank">%s</a> [<a href="%s" target="_blank" title="permalink"></a>] / ', $name == $selected ? ' selected' : '', $s['dataUrl'] . $f . '/' . $fi, $name,  cs_var('url') . 'converse/' . $f . '/' . $name);
		}
		echo '<hr />';
	}
	echo '<div id="loaded-repo"><h3 id="repo-name">[Click a Repo Link Above to view available message templates</h3><div id="repo-info"></div><hr /><div id="repo-messages"></div></div><hr />';
	$msg = 'This area will dynamically change as you select persons to include in your message and choose a template from one of them / a topic.';
	echo '<textarea rows="8" style="width: 100%" id="loaded-message">' . $msg . '</textarea>';
}
?>
