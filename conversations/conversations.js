$(document).ready(function(){
	$('.repo').click(function(e) {
		$('#repo-name').html($(this).html());
		e.preventDefault();
		$('#repo-messages').html('');
		$.ajax($(this).attr('href')).done(function (json) {
			window.currentRepo = json;
			var img = json.picture ? '<img src="' + json.picture + '" width="150" style="float: right; margin: 0 0 20px 20px;" alt="' + $('#repo-name').html() + '" /><br />' : '';
			$('#repo-info').html(img + json.about + '<br />' + (json.links ? json.links.map(function(l) { return '<a href="' + l + '" target="_blank">' + l + '</a>'; }).join('<br />') : '-- no links --') );
			$('#repo-messages').html(json.messages ? json.messages.map(function(m, ix) { return '<div class="repo-message" data-index="'+ix+'">' + m.text + '</div>'; }).join('<hr />') : '-- no messages --');
		}).fail(function (response) {
			if (response.status != '200') {
				$('#repo-info').html(reponse.statusText);
				return;
			} else if (!response.responseText) {
				$('#repo-info').html('empty file');
				return;
			}
		});
	});
	$('.repo.selected').trigger('click');
	$('#repo-messages').on('click', '.repo-message', function() {
		var ix = $(this).data('index');
		var m = window.currentRepo.messages[ix];
		$('#loaded-message').val(m.text);
	});
});
