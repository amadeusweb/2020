YieldMore - PACT Invitation

First described <a href="http://legacy.yieldmore.org/about/?node=pact" target="_blank">here</a>, PACT, the #EducationForum is an acronym representing the 4 pillars of the education system viz: Parent | Administrator | Child | Teacher, or simply "People's Alliance for children and Teachers" as Imran likes to put it.

We now invite teachers and parents of a school to share their thoughts with us both <a class="fb" href="https://www.facebook.com/groups/pactym/" target="_blank">on our FB group</a> as well as <a href="https://groups.google.com/g/pact-ym" target="_blank">on Google Groups</a>.

We will, in time have information on how to setup a Chapter / Center and have FAQs on the value PACT adds to the education sector in our <a class="g-doc" href="https://docs.google.com/document/d/1wjCEURirVrhS4ykMD22LvCAh0EewlNxN0sFVibQ123c/edit?usp=sharing" target="_blank">Definitive PACT Guide</a>.