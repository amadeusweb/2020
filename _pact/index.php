<section id="<?php echo cs_var('node'); ?>">
	<?php page_banner(); ?>
	<div class="container">
		<?php page_about() ?>
		<div class="row">
			<div class="<?php echo cs_var('sub-site-width'); ?>">
			<?php echo wpautop(file_get_contents(article_or_node_path(__DIR__ . '/' . cs_var('node') . '.txt')));?>
			</div>
		
			<div class="<?php echo cs_var('sub-site-right-col-width')?> sub-site">
				<?php if_articles(); ?>
				<div class="related-links"><?php sub_site_heading(); ?></div>
				<?php menu(['folder' => true]) ?>
			</div>
		</div>
	</div>
</section>
