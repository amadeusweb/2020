<div class="fixed-side-navbar of-section">
  <ul class="nav flex-column">
    <?php
    $home = cs_var('url') . 'happyschools/';
    foreach(cs_var('sections') as $key => $text)
        echo sprintf('<li class="nav-item"><a class="nav-link" href="%s#%s" data-target="#%s"><span>%s</span></a></li>' . cs_var('nl'), $home, $key, $key, $text);        
    ?>
    <li class="nav-item"><a class="nav-link" href="<?php echo cs_var('url'); ?>pact/"><span><- Back to YM Education Forum</span></a></li>
  </ul>
</div>
