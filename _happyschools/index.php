<style type="text/css">
<?php echo replace_vars(file_get_contents(__DIR__ . '/assets/styles.css')); ?>
</style>
<?php
cs_var('sections', $sections = [
    'home' => 'Happy Schools - Overview',
    'introduction' => 'Introduction',
    'preview' => 'Preview',
    'workshop' => 'Workshop',
    'registration' => 'Registration',
    'request-audit' => 'Request Audit',
]);
include "_menu.php";
?>

<div class="parallax-content baner-content" id="home" style="background-position: center center;">
    <div class="container">
        <div class="first-content">
            <div class="logo-container"><img src="<?php echo cs_var('url');?>_happyschools/assets/happy-schools.png<?php echo version_querystring(); ?>" alt="Happy Schools" height="200" width="300" /></div>
            <h1>Happy Schools</h1>
            <div class="primary-button">
                <a href="tel:+91-9444606456">Talk to Happy Schools</a>
            </div>
        </div>
    </div>
</div>

<?php
$subpage = cs_var('page_parameter1');
if ($subpage) {
    echo wpautop(file_get_contents(__DIR__ . '/' . $subpage . '.txt'));
    return;
}

$start = '<div class="section vanilla-section %s"><div class="container">'; $end = '</div></div>';

//echo $start . '<marquee>Our website is currently being rebuilt and will be online shortly</marquee>' . $end;

foreach (cs_var('sections') as $key => $text) {
  echo sprintf($start, $key);
  echo sprintf('<h2 id="%s">%s</h2>', $key, $text);
  echo wpautop(file_get_contents(__DIR__ . '/_' . $key . '.txt'));
  echo $end;
}
?>
