A well developed child has his physical, mental, emotional and spiritual needs met.

For this to happen, the following need to be addressed by the school.
<ol>
    <li>Physical - Infrastructure & Amenities</li>
    <li>Intellectual - Childfriendly Educators who understand and practice <a href="./tenets/">child development tenets</a>.</li>
    <li>Environment for Social Development</li>
</ol>
