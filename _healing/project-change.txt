An article by <a href="../chris/">Chris Todd Walker</a>, 24th June 2020.

Sadness, depression, loneliness and anger, etc. Not knowing how to cope or deal with a variety of issues in their lives. Any type of healing or mental/emotional improvement requires change within ones self. You have to change thoughts, feelings, habits/actions/decisions. But, none of this is ever accomplished, unless you first have acknowledged and accepted the problem. The thing hurting any one of us the most, is our inability so far, to cope and push forward through the storm, with optimism and love; particularly for ourselves and our own life. 

Taking control of your own mind and emotions through active thinking and affirmation, whenever your subconscious tells you that you're hurting deeply or affected immensely by something going on in your life, especially things you can't control. So you first have to acknowledge whatever the issue is accept it, then allow yourself to think about it and feel those feeling and then go through whatever grieving process you need to and begin working on positive thinking and Self-Love, so you can heal. 

One example of a situation where emotional healing, acceptance and self-love are needed is like below;

Issues that never seem to go away, but continue to repeat themselves. Relationship issues with a new person, but the same old problems. 

Many people in repeat situations and scenario's often times end up thinking things like, (here are a few examples) "Oh, I did everything right. What am I missing? What am I doing wrong? Why does it always end up the same way? Why do I always fail? How do I keep choosing the same type of people?" The answer is very simple, but takes a great amount of understanding to utilize. 


The issue underlying issue with all healing is in personal growth, awareness, consciousness and spiritual enlightenment. Things I aim to share with as many people as possible, so that they too may find the peace and happiness that I've finally attained in my own life and at the very least, within myself and through the same type of journey that we all must execute at some point during physical life, to fulfill the purpose that we each have set to fill for ourselves here. Whatever that purpose is, it is probably a feeling you've had deep down, all your life and just cast aside or ignored. Unaware that it is even a purpose driven, spiritual experience, many ignore their inner voice or deny it all together. Assuming it to be just a randomly occurring and fleeting thought to forget about. An inconvenience in the back of your mind. 

Why does this matter? It will. Keep reading. 


How do we heal emotionally? Acceptance. 


The first step to any emotional healing, is accepting the situation, analyzing it from various perspectives, learning what there is to be learned and making a different decision next time, that may or may not be as bad. It could be worse, but you typically won't get to any of the right decisions, if you don't make enough bad ones. 


Everyone is different, but this is true for a variety of aspects in everyone's life on some level or another. If you're experiencing the same situation or the same negativity and bad energy repeatedly, it's because you haven't learned what the Universe/God/Your higher self, is trying to teach you and you're still not in alignment with your purpose and you're suffering because of it. 


You have to grow and make new decisions. Different decisions. Otherwise, you'll be stuck in a loop of unconscious, unintentional self-destruction and only you can get yourself out. Negativity and opposing energy must be removed from your life. Situations, things, music, movies, people (even family) if these things disturb and hinder your ability to grow spiritually and emotionally. 


Growth, self-improvement, spiritual enlightenment, positive change. These are all things that can only be obtained by a person who truly desires it and truly wants to be a better version of themselves. Someone who is open to acceptance of their own shortcomings and willing to change the things about themselves that is causing them, their life and their spiritual growth harm. People who want know their reason for existing and their place in this world. 


How do we gain control over our emotions? First you need to understand "The Law Of Attraction" how it works and how thoughts and feelings work together. These are the things that create what people and religious texts call, "belief". How this works is, everything in the universe is energy and energy is attracted to like energy. So, positive attracts positive and negative attracts negative. 


What you feel on a regular basis, determines what you think. What you think on a regular basis, determines what you feel. If you think about having a relationship, but you're always sad, you will manifest a relationship that ends up making you sad. The thought manifests the situation and the feelings manifest how the situation plays out. 


To begin healing from there, you need to know why you're having the emotions you're having. You need to analyze yourself, the situation, why you feel the way you do and then understand that whether or not it's your fault, the object is to remove the negativity from your being, or from your life.


How do you remove the problem, if it's something that originates outside of you? If its a situation, remove yourself from it asap. If it's a person, try to talk about how you feel and resolve it as peacefully as possible. If that doesn't work, remove it them your life. 


How do you remove the problem, if the problem is internal? Negative emotions, thoughts, actions? First, you have to want to be better. Then you have to accept where your faults are and desire to change them. If it's thoughts that are causing you problems, change what you are thinking about by force. 


Actively and purposefully think about something else and just keep doing that everytime you realize the negative thoughts on your mind again. It takes time and repetition to change a habit, but eventually your new natural habit of thought will be more on the things you have been forcefully thinking, so think about things that would make you happy. 


How do we learn to feel the emotions we want to feel? Spiritual Growth. Meditation, prayer, seeking wisdom. Achieving growth and enlightenment can help guide nd enhance your spiritual strength, intuition, understanding and discernment. It assists in all these areas and while it takes time, deviation, practice and a true desire to raise your vibration/frequency, this is how it's done. Learning from pain and growing spiritually. As you do this, your connection with your spirit/higher self will strengthen and your thoughts and feelings will have greater effect on you and your life and even the speed at which it manifests. 

You empower what your mind and feelings give the most energy towards. You empower happiness, or you empower sadness, or fear and whatever it is that you've empowered, is what you manifest. 

Most importantly, it is detrimental to anyone seeking self-improvement, to hold on to anything egotistical, prideful, closed-mindedness, a mentality of denial of responsibility or refusal to accept both truth and change. You absolutely must be in agreement with both your feelings and your mind, that you want self-improvement. So be open to acceptance, or no amount of change can ever begin. Realization is step one and comes from meditation and self-reflection. Acceptance is step two. 

If you want change, you must be the change within yourself, so that you can project that energy and attract it in return.