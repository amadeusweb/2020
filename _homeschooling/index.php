<section id="<?php echo cs_var('node'); ?>">
	<?php page_banner(); ?>
	<div class="container">
		<?php page_about() ?>
		<div class="row">
			<div class="<?php echo cs_var('sub-site-width'); ?>">
			<?php
			$chapter = cs_var('page_parameter1') ? cs_var('page_parameter1') : '';
			$protected = strpos($chapter, '-protected') !== false;
			$content = $chapter == '_content';
			if ($protected || $content) {
				$chapter = str_replace('-protected', '', $chapter);
				echo '<div class="warning">Nice Try, Buy Below to access the protected course.</div>';
			}
			if (cs_var('page_parameter1') == 'export') include '_export.php'; else if (!$content) echo wpautop(file_get_contents(__DIR__ . '/' . (cs_var('page_parameter1') ? $chapter : cs_var('node')) . '.txt'));?>
			</div>
		
			<div class="<?php echo cs_var('sub-site-right-col-width')?> sub-site">
				<?php include __DIR__ . '/../account.php'; ?>
				<hr />
				<p class="links-inline"><b>NOTICE</b>: <?php echo replace_vars('Freely navigate the preview of the course then contact <a href="mailto:relieffoundation@gmail.com?subject=Want to purchase the Homeschooling Course from you">Relief Foundation</a> for getting access to the entire course'); ?></p>
				<hr />
				<div class="related-links"><?php sub_site_heading(); ?></div>
				<?php menu(['folder' => true, 'folder_in_url' => true, 'no_protected' => true]) ?>
			</div>
		</div>
	</div>
</section>
<script>window.access = <?php echo has_course_access(cs_var('node')) ? 'true' : 'false'; ?></script>
