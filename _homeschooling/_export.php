<?php
$file = ''; $buffer = []; $heading = ''; $course = 'homeschooling';
$nl = cs_var('nl');

$lines = explode("\r\n", file_get_contents(__DIR__ . '/_content.txt'));
$file_count = 0;
$protected = false;

echo 'Read ' . count($lines) . ' lines<br />' . $nl;

function video_link($file, $protected) {
	$cols = cs_var('export_cols');
	if ($cols) {
		$rows = cs_var('export_rows');
	} else {
		$cols = 'object';
		$rows = tsv_to_array(file_get_contents(__DIR__ . '/videos.tsv'), $cols);
		cs_var('export_cols', $cols);
		cs_var('export_rows', $rows);
	}
	foreach ($rows as $row) {
		if ($row[$cols->file] == $file && ($protected ? $row[$cols->protected] : $row[$cols->free]))
			return '<div class="video-bgd"><div class="container"><div class="video-container"><iframe title="' . $file . '" src="https://www.youtube.com/embed/' . ($protected ? $row[$cols->protected] : $row[$cols->free]) . '?feature=oembed" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div></div>';
	}
}

$assets = '"assets'; $assets_url = '"' . cs_var('url') . '_' . $course . '/assets';

foreach ($lines as $line) {
	$line = trim($line);
	//echo '--debug ' . $line . '<br />' . $nl;
	$seo = startsWith($line, 'Description:') || startsWith($line, 'Keywords:');

	if ($seo) {
		//TODO:
	} else if (startsWith($line, '#')) {
		if (count($buffer) && $file != '') {
			echo 'Wrote ' . $file . ' with '. count($buffer) . ' lines<br />' . $nl;
			file_put_contents(__DIR__ . '/' . $file . ($protected ? '-protected' : '') . '.txt', ($protected ? '<hr class="protected" />' : '<h1>' . $heading . '</h1>' . $nl) . $nl . implode($nl, $buffer));
			$buffer = []; $protected = false;
		}

		$heading = simplify_encoding(substr($line, 1));
		if ($no_count = startsWith($heading, '_') || startsWith($heading, '#')) $heading = substr($heading, 1);
		$file = ($no_count ? '' : ++$file_count . '-') . urlize(explode(':', $heading)[0]);
		$buffer[] = video_link($file, $protected);
	} else {
		$line = simplify_encoding($line);
		$line = str_replace($assets, $assets_url, $line);

		if ($line == '<!--protected-->') {
			echo 'Wrote ' . $file . ' with '. count($buffer) . ' lines<br />' . $nl;
			$protected = true;
			$buffer[] = '<div class="protected"><a href="mailto:relieffoundation@gmail.com?subject=BUY COURSE on YieldMore.org">BUY COURSE</a>';
			$buffer[] = '<button data-subnode="' . $file .'" data-course="' . $course .'" class="buy hidden">BUY COURSE</button></div>';
			file_put_contents(__DIR__ . '/' . $file . '.txt', '<h1>' . $heading . '</h1>' . $nl . $nl . implode($nl, $buffer));
			$buffer = [];
			$buffer[] = video_link($file, $protected);
			continue;
		}

		$buffer[] = $line;
	}

}

echo 'Wrote ' . $file_count . ' files<br />' . $nl;

?>
