<?php
$prefix = 'csf2021-flyer';
$flyer = $prefix . (cs_var('page_parameter1') ? cs_var('page_parameter1') : 'YM');
?>
<section id="<?php echo cs_var('node'); ?>">
	<?php page_banner(); ?>
	<div class="container">
		<?php page_about() ?>
		<div class="row">
			<div class="<?php echo cs_var('sub-site-width'); ?>">
			<a href="https://storytellinginstitute.org/CSF2021.html" target="_blank" class="silent-external-link">
				<img class="img-fluid" src="<?php echo cs_var('url') . '_explore/csf2021/' .$flyer . '.jpg'; ?>" />
			</a>
			<?php echo wpautop(file_get_contents(__DIR__ . '/' . cs_var('node') . '.txt'));?>
			</div>
		
			<div class="<?php echo cs_var('sub-site-right-col-width')?> sub-site">
				<div class="related-links">Chennai Storytelling Festival 2021</div>
				Media Kit:
				<ol><?php $flyers = ['YM', 'A', 'B', 'C', 'D'];
				foreach($flyers as $f) {?>
					<li><?php echo sprintf('<a href="%s%s/%s/">Flyer %s</a>', cs_var('url'), cs_var('node'), $f, $f); ?></li><?php } ?>
				</ol>
				<?php echo wpautop(file_get_contents(cs_var('path') .  '/' . cs_var('folder') . '/' . cs_var('node') . '/_sidebar.txt'));?>
			</div>
		</div>
	</div>
</section>
