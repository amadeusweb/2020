<style type="text/css">
.am-form input, .am-form textarea { width: 100%; }
.am-form textarea { height: 80px; }
.am-form h1 { font-size: 20pt!important; margin-top: 60px; }
.am-form h2 { font-size: 16pt; text-decoration: underline; font-weight: bold; margin-top: 30px; }
.am-form h3 { font-size: 12pt; margin-top: 15px; }
</style>

<?php
$cols = 'object';
$fields = tsv_to_array(file_get_contents(__DIR__ . '/form.tsv'), $cols);
$nl = cs_var('nl');

if (count($_POST)) {
	$text = '<!--Submission To YieldMore.org dated ' . date('d M Y') . '-->' . $nl . $nl;
	foreach ($_POST as $key => $value)
	{
		$key = str_replace('_', ' ', $key);
		if ($value == 'section') {
			$text .= '<h2>' . $key . '</h2>'. $nl . $nl;
		} else {
			if (!$value) continue;

			if (endsWith($key, '*ml'))
				$text .= '<h3>' . str_replace('*ml', '', $key) . '</h3>'. $nl;
			else
				$text .= '<u>' . $key . '</u>'. $nl;
			$text .= $value. $nl . $nl;
		}
	}

	$file = strtotime("now");
	file_put_contents(sprintf('%s/%s-%s.txt', __DIR__, $file, $_POST['Name']), $text);
	echo 'Please email this number: ' . $file . ' to <a href="mailto:cwc@yieldmore.org">cwc@yieldmore.org</a> and include a picture</a>.';
} else {
	$html = '<form method="post" class="am-form">' . $nl;
	$html .= '<h1>Submission To YieldMore.org</h2>' . $nl . $nl;
	$html .= '<p>Please fill in only details you are comfortable giving.</p>' . $nl . $nl;

	$section = false;
	foreach ($fields as $f)
	{
		if ($section != $f[$cols->Section]) {
			$section = $f[$cols->Section];
			$html .= '<input type="hidden" name="' . $section . '" value="section">' . $nl;
			$html .= '<h2>' . $section . '</h2>'. $nl . $nl;
		}

		$html .= '<h3>' . $f[$cols->Field] . '</h3>'. $nl . $nl;
		if ($f[$cols->Type] == 'multiline-text')
			$html .= '<textarea name="'. $f[$cols->Field] .'*ml"></textarea>'. $nl . $nl;
		else
			$html .= '<input type="text" name="'. $f[$cols->Field] .'" />'. $nl . $nl;
		
		if ($f[$cols->SubmitHint])
			$html .= '<small>'. $f[$cols->SubmitHint] .'</small>'. $nl . $nl;
	}
	$html .= '<input type="submit" value="Save Details" style="margin-top: 20px;" />' . $nl;
	$html .= '</form>' . $nl;

	echo $html;
}
?>
